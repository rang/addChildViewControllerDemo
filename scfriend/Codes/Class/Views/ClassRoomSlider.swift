//
//  ClassRoomSlider.swift
//  scfriend
//
//  Created by wulanzhou on 2017/7/26.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

import UIKit

class ClassRoomSlider: UIView {
    
    private var currentIndex = 0
    
    typealias SliderSelectedItemIndexBlock = (_ selectedIndex:Int) -> ()
    var selectedItemIndexBlock:SliderSelectedItemIndexBlock?
    
    //MAKE:懒加载
    private lazy var lineLabel:UILabel = {
    
        let lab:UILabel = UILabel.init(frame: CGRect(x: self.width/2.0, y: 19, width: 0.5, height: (self.height - 38)))
        lab.backgroundColor = UIColor.withRGBA(51, 51, 51)
        return lab
        
    }()
    
    private lazy var sliderLabel:UILabel = {
        
        let lab:UILabel = UILabel.init(frame: CGRect(x: 0, y: self.height - 3, width: self.width/2.0, height: 3))
        lab.backgroundColor = UIColor.withRGBA(42, 173, 240)
        return lab
        
    }()
    

    private lazy var roomButton:UIButton = {
    
        let btn:UIButton = UIButton.init(type: .custom)
        btn.frame = CGRect(x: 0, y: 0, width: self.width/2.0, height: self.height)
        btn.setTitle("课堂", for: .normal)
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.setTitleColor(UIColor.withRGBA(42, 173, 240), for: .highlighted)
        btn.adjustsImageWhenHighlighted = true
        btn.tag = 100
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
        btn.addTarget(self, action: #selector(menuClick(sender:)), for: .touchUpInside)
        return btn
    }()
    
    private lazy var groupButton:UIButton = {
        
        let btn:UIButton = UIButton.init(type: .custom)
        btn.frame = CGRect(x: self.width/2.0, y: 0, width: self.width/2.0, height: self.height)
        btn.setTitle("小组", for: .normal)
        btn.setTitleColor(UIColor.black, for: .normal)
        btn.setTitleColor(UIColor.withRGBA(42, 173, 240), for: .highlighted)
        btn.adjustsImageWhenHighlighted = true
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
        btn.tag = 101
        btn.addTarget(self, action: #selector(menuClick(sender:)), for: .touchUpInside)
        return btn
    }()
    
    @objc fileprivate func menuClick(sender:UIButton){
    
        if sender.tag - 100 != currentIndex {
        
            UIView.animate(withDuration: 0.1, animations: {
                sender.setTitleColor(UIColor.withRGBA(42, 173, 240), for: .normal)
                (self.viewWithTag(100 + self.currentIndex) as? UIButton)?.setTitleColor(UIColor.black, for: .normal)
                self.sliderLabel.x = CGFloat(sender.tag - 100)*(self.width/2.0)
            }, completion: { (finished) in

                if finished {
                   self.currentIndex = sender.tag - 100
                   self.selectedItemIndexBlock?(self.currentIndex)
                }
            })
        
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        self.addSubview(self.roomButton)
        self.addSubview(self.groupButton)
        self.addSubview(self.lineLabel)
        self.addSubview(self.sliderLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
