//
//  RoomGroupViewController.swift
//  scfriend
//
//  Created by wulanzhou on 2017/7/26.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

import UIKit

class RoomGroupViewController: UIViewController {
    
    
    private  var isTransition = false
    
    //MAKE:懒加载
    private  var controllerList:NSMutableArray = {
    
        let mul = NSMutableArray.init(capacity: 0)
        mul.add(NSNull.init())
        mul.add(NSNull.init())
        return mul
    }()

    private lazy var sliderView:ClassRoomSlider = {
        
        weak var WSELF = self
        let slider = ClassRoomSlider.init(frame: CGRect(x: 0, y: 0, width: self.view.width, height: 50))
        slider.selectedItemIndexBlock = { (index) in
            WSELF?.transitionView(index: index)
        }
        return slider
        
    }()
    
    //两个viewController切换处理
    private func transitionView(index:Int){
        
        self.replaceControllerForIndex(index: index)
        
        if  let vc = self.childViewControllers.last{
            if !vc.isEqual(self.controllerList[index]) {
            
                if(isTransition){
                    return
                }
                isTransition = true
                let toVc = self.controllerList[index] as! UIViewController
                self.addChildViewController(toVc)
                transition(from: vc, to: toVc, duration: 0.1, options: UIViewAnimationOptions.curveEaseInOut, animations: nil, completion: { (finished) in
                    
                    if finished {
                        vc.didMove(toParentViewController: nil)
                        vc.removeFromParentViewController()
                        vc.view.removeFromSuperview()
                        
                        self.view.addSubview(toVc.view)
                        toVc.didMove(toParentViewController: self)
                        self.isTransition = false
                    }
                    
                })
            
            }
        
        }

    }
    
    private func replaceControllerForIndex(index:Int)  {
        
        if (self.controllerList[index] as AnyObject).isKind(of:NSNull.self) {
            let groupVc = UIViewController()
            groupVc.view.frame = CGRect(x: 0, y: self.sliderView.frame.maxY, width: self.view.width, height: self.view.height - self.sliderView.frame.maxY)
            groupVc.view.backgroundColor = UIColor.randomColor()
            self.controllerList[index] = groupVc
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "demo"
        self.edgesForExtendedLayout = []
        self.view.backgroundColor = UIColor.white
        self.view.addSubview(self.sliderView)
       
        //默认显示第一个
        self.replaceControllerForIndex(index: 0)
        if let roomVC = self.controllerList[0] as? UIViewController {
            self.addChildViewController(roomVC)
            self.view.addSubview(roomVC.view)
            roomVC.didMove(toParentViewController: self)
        }
   
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}
