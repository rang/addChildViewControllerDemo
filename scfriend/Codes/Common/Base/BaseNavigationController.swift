//
//  BaseNavigationController.swift
//  scfriend
//
//  Created by wulanzhou on 2017/7/18.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

import UIKit

class BaseNavigationController: UINavigationController {

    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //修改导航栏背景色
        self.navigationBar.barTintColor = UIColor.withRGBA(42, 173, 240)
        //修改导航栏文字颜色
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //修改导航栏按钮颜色
        self.navigationBar.tintColor = UIColor.white
        
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
