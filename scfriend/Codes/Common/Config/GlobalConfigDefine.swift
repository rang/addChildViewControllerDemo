//
//  GlobalConfigDefine.swift
//  shopDemo
//
//  Created by wulanzhou on 2017/7/7.
//  Copyright © 2017年 wulanzhou. All rights reserved.
//

import Foundation
import UIKit

//--------------------------服务器地址--------------------------//





//--------------------------常量定义--------------------------//
let ScreenRect:CGRect = UIScreen.main.bounds
let ScreenWith = ScreenRect.size.width
let ScreenHeight = ScreenRect.size.height
let ScreenSizeScaleX = (ScreenHeight > 480 ? ScreenHeight/320.0:1.0)
let ScreenSizeScaleY = (ScreenHeight > 480 ? ScreenHeight/568.0:1.0)
let kTabBarHeight:CGFloat = 49.0
let kNavigationBarHeight:CGFloat = 64.0
let kStatusBarHeight:CGFloat = 20.0
let kTabBarItemNormalFontColor = UIColor.init(red: 128/255.0, green: 128/255.0, blue: 128/255.0, alpha: 1.0)
let kTabBarItemSelectedFontColor = UIColor.init(red: 252/255.0, green: 46/255.0, blue: 57/255.0, alpha: 1.0)

